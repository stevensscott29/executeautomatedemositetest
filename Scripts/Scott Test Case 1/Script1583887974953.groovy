import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

for (def row = 1; row <= findTestData('Scott Test Data').getRowNumbers(); row++) {
    WebUI.openBrowser('')

    WebUI.navigateToUrl('http://www.executeautomation.com/demosite/Login.html')
	Login(findTestData('Scott Test Data').getValue('UserName', row),findTestData('Scott Test Data').getValue(
            'Password', row))

    WebUI.click(findTestObject('Object Repository/Page_Execute Automation/input_Login_Login'))

    WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Execute Automation/select_SelectMrMs'), '1', true)

    WebUI.setText(findTestObject('Object Repository/Page_Execute Automation/input_Initial_Initial'), findTestData('Scott Test Data').getValue(
            'Initial', row))

    WebUI.setText(findTestObject('Object Repository/Page_Execute Automation/input__FirstName'), findTestData('Scott Test Data').getValue(
            'Fname', row))

    WebUI.setText(findTestObject('Object Repository/Page_Execute Automation/input_Middle Name_MiddleName'), 'MiddleName')

    WebUI.click(findTestObject('Object Repository/Page_Execute Automation/input_EnglishHindi_Hindi'))

    WebUI.click(findTestObject('Object Repository/Page_Execute Automation/input_EnglishHindi_Save'))

    WebUI.click(findTestObject('Object Repository/Page_Execute Automation/a_HtmlPopup'))

    WebUI.switchToWindowTitle('Popup Window')

    WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Popup Window/select_SelectMrMs'), '1', true)

    WebUI.click(findTestObject('Page_Popup Window/input_Initial_Initial'))

    WebUI.setText(findTestObject('Page_Popup Window/input__FirstName'), 'scottscott')

    WebUI.setText(findTestObject('Page_Popup Window/input_Middle Name_MiddleName'), 'in ')

    WebUI.setText(findTestObject('Page_Popup Window/input__LastName'), 'popup')

    WebUI.switchToWindowIndex('0')

    WebUI.setText(findTestObject('Object Repository/Page_Execute Automation/input_Initial_Initial'), 'SS')

    WebUI.setText(findTestObject('Object Repository/Page_Execute Automation/input__FirstName'), 'Bob')

    WebUI.setText(findTestObject('Object Repository/Page_Execute Automation/input_Middle Name_MiddleName'), 'Joe')

    WebUI.closeBrowser()
}

def Login(def userName, def password) {
    WebUI.setText(findTestObject('Object Repository/Page_Execute Automation/input_Login_UserName'), userName)

    WebUI.setText(findTestObject('Object Repository/Page_Execute Automation/input_Login_Password'), password)
}